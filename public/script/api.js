const baseURl = 'https://jsonplaceholder.typicode.com/'

const status =  (response) => {
        if (response.status >= 200 && response.status < 300) {
            return response;
        } else {
            return Promise.reject(new Error( response.status + ' ' + response.statusText))
        }
}
const json = (response) => {
    return response.json()
}


const api = {
    getUsers:  () => {
      fetch(baseURl + 'users')
           .then(status)
           .then(json)
          .then(response => setUsers(response))
           .catch(error => {
               displayError(error.message, 'listUsers')
           });

    },
    getUserPosts: (userId) => {
        fetch(baseURl + `posts?userId=${userId}`)
            .then(status)
            .then(json)
            .then(response => setPosts(response))
            .catch(error => {
                displayError(error.message, 'listPosts')
            });
    }
}
