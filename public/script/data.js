const useState =  (functionRender) => {
    let state
    const set =  users => {
        state = users
        functionRender(users)
    }
    const get = () => {
        return state;
    }
    return [get,set]
}
