function display(local, data, createItem) {
    const location = $(local)
    location.innerHTML = '';
    data.forEach(createItem(location))
}
function createUserBlock(location){
    return (u) => {
        let li = document.createElement('li')
        li.className = "user-block"
        li.innerHTML = `<a href="#" id="userId-${u.id}">${u.name}</a>`
        location.append(li)
    }
}
function createPostBlock(location){
    location.style.maxHeight = '900px';
    return (p) => {
        let li = document.createElement('li')
        li.className = "post-block"
        li.innerHTML = `<h3 id="postId-${p.id}">${p.title}</h3> <p>${p.body}</p>`
        location.append(li)
    }
}
function  displayError(error, local) {
    const location = $('#'+local);
    location.textContent = '';
    let div = document.createElement('div')
    div.className = "error_block"
    div.innerHTML = `<p> Error: ${error} </p>`
    location.append(div)
}

function displayUsers(users) {
    display('#listUsers', users, createUserBlock)
}
function displayPosts(posts) {
    display('#listPosts', posts, createPostBlock)
}