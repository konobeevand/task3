function $(selector) {
    return document.querySelector(selector)
}
const preloader = '<div class="container">\n' + '  <div class="📦"></div>\n' + '  <div class="📦"></div>\n' + '  <div class="📦"></div>\n' + '  <div class="📦"></div>\n' + '  <div class="📦"></div>\n' + '</div>'
const [users, setUsers] = useState(displayUsers)
const [posts, setPosts] = useState(displayPosts)

$('#listUsers').innerHTML = preloader
api.getUsers();

const changeActive = (e) => {
    if($('.active')){
        $('.active').classList.remove('active')
    }
    e.target.classList.add('active')
}

$('#listUsers').addEventListener('click', (e) => {
    e.preventDefault()
    changeActive(e)
    const userId = e.target.id.slice(7)
    const listPosts = $('#listPosts');
    listPosts.style.maxHeight = '100px';
    listPosts.innerHTML = preloader
    api.getUserPosts(userId);
})

